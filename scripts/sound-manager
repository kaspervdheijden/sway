#!/usr/bin/env sh
# dependency=pactl

if ! "${WM_CONFIG_DIR}/scripts/util" is-executable pactl jq; then
    exit 127
fi


friendly_name() {
    pactl -f json list sinks | jq -r --arg item "${1}" '.[] | select(.name == $item) | .properties."node.nick", .description' | sed '/^$/d; /^null$/d' | head -n1
}

get_available_sinks() {
    pactl -f json list sinks | jq -r '.[].name'
}

signal_panel() {
    if "${WM_CONFIG_DIR}/scripts/util" is-running i3status; then
        killall -SIGUSR1 i3status
    fi

    if "${WM_CONFIG_DIR}/scripts/util" is-running waybar; then
        pkill -RTMIN+8 waybar
    fi
}


case "${1}" in
    'set-volume')   pactl set-sink-volume @DEFAULT_SINK@ "${2:-10}%" && signal_panel ;;
    'toggle-mute')  pactl set-sink-mute   @DEFAULT_SINK@ toggle      && signal_panel ;;
    'select-output')
        sinks="$(get_available_sinks)"
        if [ -z "${sinks}" ]; then
            printf 'No sinks\n' >&2
            exit 3
        fi

        current="$(pactl get-default-sink)"
        mapping="$(printf '%s\n' "${sinks}" | while read -r sink; do printf '%s:%s\n' "${sink}" "$(friendly_name "${sink}")"; done)"
        chosen="$( \
            printf '%s\n' "${mapping}" | \
            awk -F':' -v cur="${current}" '{ s = ""; if ($1 == cur) { s = "* "; } print s$2; }' | \
            sort -r | \
            "${WM_CONFIG_DIR}/scripts/selector" --select-mode 'Select output device: ' |
            sed 's/^* //' \
        )"

        if [ -z "${chosen}" ]; then
            exit
        fi

        sink="$(printf '%s\n' "${mapping}" | awk -F':' -v chosen="${chosen}" '$2 == chosen { print $1; }' | head -n1)"
        if [ -z "${sink}" ]; then
            printf 'Chosen sink name not found for "%s"\n' "${chosen}" >&2
            exit 4
        fi

        if [ "${sink}" = "${current}" ]; then
            printf 'Already set to "%s"\n' "${sink}"
        elif ! pactl set-default-sink "${sink}"; then
            printf 'Error setting default sink to "%s"\n' "${sink}" >&2
            exit 5
        fi

        signal_panel
        ;;
    'startup')
        sinks="$(get_available_sinks)"
        if [ -z "${sinks}" ]; then
            exit
        fi

        item="$(printf '%s\n' "${sinks}" | grep -E '^bluez_output\.')"
        if [ -z "${item}" ]; then
            item="$(printf '%s\n' "${sinks}" | tail -n1)"
        fi

        pactl set-default-sink "${item}"

        ;;
    *)
        printf 'Unrecognized command "%s"\n' "${1}" >&2
        exit 6

        ;;
esac
