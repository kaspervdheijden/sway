#!/usr/bin/env sh
# dependency=column

chooseEntry() {
    pass_name="${1}"
    if [ -n "${pass_name}" ]; then
        printf '%s\n' "${pass_name}"
        return
    fi

    files="$("${WM_CONFIG_DIR}/scripts/pass" list 2>/dev/null)"
    if [ -z "${files}" ]; then
        "${WM_CONFIG_DIR}/scripts/notify" 'Password manager' 'No passwords defined!'
        printf 'No passwords defined\n' >&2
        return 1
    fi

    pass_name="$(printf '%s\n' "${files}" | "${WM_CONFIG_DIR}/scripts/selector" --select-mode 'Entry')"
    if [ -z "${pass_name}" ]; then
        return 1
    fi

    printf '%s\n' "${pass_name}"
}

action="${1}"
case "${action}" in
    has)
        "${WM_CONFIG_DIR}/scripts/pass" list 2>/dev/null | grep -qE "^${2}$"
        ;;
    output-if-cached)
        pass_name="$(chooseEntry "${2}")"
        [ -z "${pass_name}" ] && exit 7

        "${WM_CONFIG_DIR}/scripts/pass" show-if-cached "${pass_name}" || exit 2
        ;;
    copy)
        pass_name="$(chooseEntry "${2}")"
        [ -z "${pass_name}" ] && exit 7

        export PASS_CLIP_TIMEOUT
        PASS_CLIP_TIMEOUT=5

        "${WM_CONFIG_DIR}/scripts/pass" copy "${pass_name}" || exit 2
        "${WM_CONFIG_DIR}/scripts/notify" 'Password' "Copied password for '${pass_name}' for ${PASS_CLIP_TIMEOUT} seconds"
        ;;
    output)
        pass_name="$(chooseEntry "${2}")"
        [ -z "${pass_name}" ] && exit 7

        "${WM_CONFIG_DIR}/scripts/pass" show "${pass_name}" || exit 2
        ;;
    interactive)
        printf 'PASS interactive shell. Type "h" for help.\n> '
        while read -r cmd arg1 arg2 arg3 arg4; do
            case "${cmd}" in
                q|qu|qui|quit|e|ex|exi|exit)
                    exit
                    ;;
                h|he|hel|help)
                    "${WM_CONFIG_DIR}/scripts/pass" 'help' 1 | while read -r cm desc; do
                        printf '%-24s %s\n' "${cm}" "${desc}"
                    done
                    ;;
                '') ;;
                *)
                    "${WM_CONFIG_DIR}/scripts/pass" "${cmd}" "${arg1}" "${arg2}" "${arg3}" "${arg4}"
                    ;;
            esac

            printf '> '
        done || true
        ;;
    ls|list)
        "${WM_CONFIG_DIR}/scripts/pass" list 2>/dev/null
        ;;
    *)
        printf 'Invalid action "%s"\n' "${action}" >&2
        exit 8
        ;;
esac
