Jumbo success::success3_0_0::success3_0_1::success3_0_2:
Jumbo success::success3_1_0::success3_1_1::success3_1_2:
Jumbo success::success3_2_0::success3_2_1::success3_2_2:
Jumbo kekw::kekw_0_0::kekw_1_0:
Jumbo kekw::kekw_0_1::kekw_1_1:
Jumbo sem::mes_0_0::mes_1_0:
Jumbo sem::mes_0_1::mes_1_1:
Mega kekw::ris_0_0::ris_1_0::ris_2_0:
Mega kekw::ris_0_1::ris_1_1::ris_2_1:
Mega kekw::ris_0_2::ris_1_2::ris_2_2:
Mega kekw::ris_0_3::ris_1_3::ris_2_3:
Mega kekw::ris_0_4::ris_1_4::ris_2_4:
Blij::blij_1::blij_2:
Blij::blij_3::blij_4:
Paid::paid_0_0::paid_1_0::paid_2_0:
Paid::paid_0_1::paid_1_1::paid_2_1:
Shell shebang:#!/usr/bin/env sh
Shell shebang:
Php script:#!/usr/bin/env php
Php script:<?php
Php script:
Php script:declare(strict_types=1);
Php script:
Php script:require_once __DIR__ . '/ScriptBase.php';
Php script:
Php script:(
Php script:    new class($argv) extends ScriptBase
Php script:    {
Php script:        public function run(): void
Php script:        {
Php script:            // magic
Php script:        }
Php script:    }
Php script:)->run();
