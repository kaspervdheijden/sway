#!/usr/bin/env sh

target="${1:-${WM_NAME}}"
if [ -z "${target}" ]; then
    target="${DESKTOP_SESSION}"
fi

if [ -z "${target}" ]; then
    printf 'No target given\n' >&2
    exit 10
fi

cd "$(dirname "${0}")/.." || exit 11
if [ ! -d "./wm/${target}" ]; then
    printf 'Invalid target: %s\n' "${target}" >&2
    exit 11
fi

if ! ./scripts/util check-online -p; then
    exit 12
fi

# shellcheck disable=SC1091
. ./scripts/environment

#
# Start with a clean worktree
#
printf 'setup for %s...' "${target}"

git fetch --all >/dev/null && git checkout -- . >/dev/null && git reset --hard >/dev/null
old_commit="$(git rev-parse --verify HEAD)"

if ! git pull --quiet --stat --force --allow-unrelated-histories --rebase; then
    git rebase --abort
    printf ' failed to pull\n' >&2
    exit 13
else
    printf ' OK\n'
fi


#
# Checking for dependencies
#
printf 'checking dependencies\n'
{ git grep '^# dependency='; git grep "^# dependency\[${target}\]="; } | cut -d= -f2- | sort | uniq | while read -r requirement
do
    if printf '%s\n' "${requirement}" | grep -qE '^file//'; then
        file="$(printf '%s\n' "${requirement}" | sed 's#^file//##')"
        if [ ! -f "${file}" ]; then
            printf '  -> WARN: required file not found: "%s"\n' "${file}" >&2
        fi
    elif printf '%s\n' "${requirement}" | grep -qE '^font//'; then
        font="$(printf '%s\n' "${requirement}" | sed 's#^font//##')"
        if ! fc-list | grep -q "${font}"; then
            printf '  -> WARN: required font not found: "%s"\n' "${font}" >&2
        fi
    elif printf '%s\n' "${requirement}" | grep -qF '|'; then
        if [ -z "$(printf '%s\n' "${requirement}" | tr '|' '\n' | xargs -I{} sh -c 'command -v "{}"')" ]; then
            printf '  -> WARN: no required binary found in [%s]\n' "$(printf '%s\n' "${requirement}" | sed 's/|/, /g')"
        fi
    elif ! ./scripts/util is-executable -q "${requirement}"; then
        printf '  -> WARN: required binary not found: "%s"\n' "${requirement}" >&2
    fi
done

#
# Install config files
#
printf 'installing config files\n'
sed '/^#/d; /^$/d' ./common/setup/config.conf | while read -r source destination required_binary; do
    if ! command -v "${required_binary:-true}" >/dev/null; then
        continue
    fi

    path_name="$(dirname "${HOME}/${destination}")"
    if [ ! -d "${path_name}" ] && ! mkdir -p "${path_name}"; then
        printf 'Could not create %s\n' "${path_name}" >&2
        continue
    fi

    printf '  -> setup %s to ~/%s\n' "${source}" "${destination}"
    cp "./common/setup/config/${source}" "${HOME}/${destination}"
done


#
# Install plugins
#
printf 'installing external repositories\n'

disabled_plugins="$(./scripts/get-config 'disabled-plugins' '')"
while read -r repo_name repo_dir repo_url update_command; do
    if printf ';%s;\n' "${disabled_plugins}" | sed 's/ //g' | grep -qF ";${repo_name};"; then
        if  [ -d "./external/${repo_dir}" ]; then
            printf '  -> purging %s\n' "${repo_name}"
            rm -rf "./external/${repo_dir}"
        else
            printf '  -> skipping %s\n' "${repo_name}"
        fi

        continue
    fi

    if [ ! -d ./external ] && ! mkdir ./external; then
        printf '  -> could not create ./external dir\n' >&2
        break
    fi

    if [ -d "./external/${repo_dir}" ]; then
        printf '  -> updating %s\n' "${repo_name}"
    else
        printf '  -> cloning %s\n' "${repo_name}"

        if ! git clone --quiet "${repo_url}" "./external/${repo_dir}"; then
            printf '  -> FAILED to clone\n'
            continue
        fi
    fi

    (
        cd "./external/${repo_dir}" && {
            if [ -n "${update_command}" ] && [ -f "./${update_command}" ]; then
                "${update_command}"
            else
                git pull --quiet --force --allow-unrelated-histories --rebase
            fi
        }
    )
done < ./common/setup/external-repos.txt


#
# Reload if changed
#
if printf '%s' "${2}" | grep -qF 'r' || [ "${old_commit}" != "$(git rev-parse --verify HEAD)" ]; then
    "./scripts/command" 'reload' >/dev/null
fi

#
# Wait if wanted
#
if printf '%s' "${2}" | grep -qF 'w'; then
    printf '\n'
    "./scripts/util" counter 10 'Auto close in %d...'
fi
