## Sway/i3/Hyprland config

These are my sway/i3/hyprland configurations. It works well in combination with
my [dotfiles](https://gitlab.com/kaspervdheijden/dotfiles.git)
and [wallpapers](https://gitlab.com/kaspervdheijden/wallpapers.git) repo's.

![preview](./common/image/preview.png)


## Dependencies
The dependencies are split into 2 categories; those required by all window managers,
and those required by specific window managers. Running the setupscript will check
dependencies for the specific window manager.

The install script checks if the dependencies exist, but will _not_ install any.
installed, too. To prevent this, add `disabled-plugins=dotfiles;` to the
In addition, [external repositories](./common/setup/external-repos.txt) will be
`$WM_CONFIG_VARS` environment variable.


## Installation
 1. Install all dependencies mentioned above
 2. `git clone https://gitlab.com/kaspervdheijden/sway.git "${XDG_CONFIG_HOME:-$HOME/.config}/wm"`
 3. `"${XDG_CONFIG_HOME:-$HOME/.config}/wm/scripts/install" 'sway'`


## Package upgrades in panel
These configs support system-updates being shown in the panel. To get this to work
you need a cronjob under root, or have it run as a systemd service.

Please see the [update manager](./scripts/update-manager) for more information on how to do this.


## Environment variables
There are two environment variables in use:
 1. `$BROWSER` Used to launch a browser.
 2. `$EDITOR` Used to launch an editor (for the scratchpad).
 3. `$WM_CONFIG_DIR` This points to the root dir for this repo. This gets set at runtime.
 4. `$WM_CONFIG_VARS` Use to control certain elements. This needs to be set prior to
  starting the windowmanager.


The variable `$WM_CONFIG_VARS` should start with a separator char which is used to delimit variables.
These variables are currently supported:
 1. `disabled-plugins` which of the items in [external repositories](./common/setup/external-repos.txt) to ignore
 2. `launcher` The preferred launcher/selector used. Either rofi, wofi, dmenu or fzf are supported
 3. `display-lid` The output name for with to disable the monitor when a lid is closed
 4. `idle` used to control idle inhibitors. Prevents swayllock from running when set and matched;
   1. Prefixed with `ssid` to match on a wifi ssid, e.g: `idle=ssid:your ssid here`
   3. Prefixed with `ip:` to match on an IP, eg.:`idle=ip:168.58.27.1`
 5. `password-name-vpn` The VPN password name to get from [the password store](./scripts/pass)
 6. `password-name-sudo` The root password name to get from [the password store](./scripts/pass)
 7. `vpn-name` Select a VPN name if multiple exist.

You can override any of these shell variable in the `.env` file in the root of this repository.


## Emoji support in panel
To get emoji's to render properly you need to install the noto-color-emoji package. After
installing the font, copy the [font.conf](./common/fonts.conf) file to `$XDG_CONFIG_HOME/fontconfig`:

```sh
mkdir -p "${XDG_CONFIG_HOME:-${HOME}/.config}/fontconfig" && \
    cp ./common/fonts.conf "${XDG_CONFIG_HOME:-${HOME}/.config}/fontconfig/fonts.conf"
```

## Install / update
To install and update this repo, simply call `./scripts/install`. You can also use the keyboard shortcut
`super+F9`. It attempts to detect the window manager using the `$DESKTOP_SESSION` variable. This
behaviour can be overwritten by manually passing either `i3` or `sway` as an argument.


## Running from tty
You can run sway/i3/hyprland from a login-manager, but you can also disable a login manager
and start the environment directly from a tty.

To launch directly from a TTY, first disable the login-manager (gdm in this example):
```sh
sudo systemctl disable gdm.service
```

To launch from a TTY, put this in a shell startup file: (E.g.: `.profile`, `.bashrc`, `.zshrc`)
```sh
if [ -z "${DISPLAY}" ] && [ "$(tty)" = '/dev/tty1' ]; then
    start_wm() {
        env WM_CONFIG_VARS="|display-lid=eDP-1" "${XDG_CONFIG_HOME:-${HOME}/.config}/wm/scripts/start-wm" "${1}"
        printf 'to start %s, type start_wm "%s"\n' "${1}" "${1}"
    }

    # start sway
    start_wm 'sway'
fi
```

Optionally, you can make sure you don't have to enter your username on `tty1`
(you still need to enter your password).

```sh
sudo mkdir -p /etc/systemd/system/getty@tty1.service.d && cat << OEF | sudo tee /etc/systemd/system/getty@tty1.service.d/skip-username.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -- $(whoami)' --noclear --skip-login - \$TERM
Type=simple
OEF
```
